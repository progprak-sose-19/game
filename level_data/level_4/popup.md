## Level 4: Spielserver

Ein Spielunternehmen hat wegen eines Patches die Wut von Spielern ihres MMORPG entfacht.
Seitdem werden die Anmelde- und Spielserver durch **DDoS**-Angriffen lahmgelegt,
sodass das Unternehmen täglich Spieler und Geld verliert!

Gebraucht werden leistungsstarke **Spielserver** die vom Anmeldeservice **abgekoppelt** sind.
Die registrierten Spieler werden mit Account und verschlüsseltem Passwort gespeichert.
Nach außen soll das System robust gegen DDoS-Angriffen sein. Außerdem soll der **Punktestand** der Spieler
gespeichert werden.

Wenn Sie oben auf ihre aktuelle **Punktzahl** klicken, sehen Sie wie ihr Score berechnet wurde.
Sie können jederzeit ein Level erneut spielen, um einen besseren Score zu erhalten.

Fangen Sie an.
