## Level 2: Fotostudio

Ein Fotograf möchte die **Bilder** als auch die Informationen seiner Kunden,
wie z.&nbsp;B. die Personalien, die **Aufträge** und eventuelle Anmerkungen, über uns verwalten
und den Kunden zur Verfügung stellen. Die vom Fotografen geschossenen Bilder sind große Dateien.
Die Kundeninformationen nicht. Überlegen Sie sich welche **Datenbank** für welche **Kategorie** geeignet ist.

Gehen Sie oben rechts zu den Services. Jeder Service hat in der oberen rechten Ecke ein **Info-Button**.
Klicken Sie darauf, um mehr über den jeweiligen Services zu erfahren.
Das Fenster können Sie bei Bedarf unten rechts wieder schließen.

Legen Sie los!
