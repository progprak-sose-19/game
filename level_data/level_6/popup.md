## Level 6: E-Mail-Empfang

Ein Internetprovider möchte die empfangenen **E-Mails** automatisch Mitarbeitern aus der richtigen Abteilung zustellen.
Beispielsweise sollen Spam-Mails herausgefiltert werden und Bestellungen, Kündigungen, Störungen
direkt an die **zuständigen Mitarbeiter** weitergeleitet werden.

Für die Klassifizierung soll Machine Learning mit Tensorflow implementiert werden.

Wenn Sie oben rechts auf ihre aktuelle **Punktzahl** klicken, sehen Sie wie ihr Score berechnet wurde.
Sie können jederzeit ein Level erneut spielen, um einen besseren Score zu erhalten.

Um wirklich alle Services sehen zu können, müssen Sie manchmal durch die Services oben rechts **durchscrollen**.
