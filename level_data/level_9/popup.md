## Level 9: Machine Learning Wetterdaten

Für die Verbesserung von Wettervorhersagen wollen
Wissenschaftler viele Daten sammeln und mithilfe von machine learning bessere Prognosen erstellen.

Die aktuellen Wetterdaten werden bereits durch eine Datenbank bereitgestellt.
Um kosten zu sparen, werden die Wetterdaten in einen **Data Lake** gesammelt.
Die gespeicherten Daten sollen in einem Warehouse eingebunden werden.
Bei Bedarf werden die Daten analysiert, um Wetterprognosen zu erstellen.
Je mehr Daten analysiert werden, desto akkurater werden die Vorhersagen.
