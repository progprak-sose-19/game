## Lambda für Empfängerdaten

AWS Lambda ist ein Dienst, der eigenen Code ausführt, ohne dass man sich um die
unterliegende Server-Infrastruktur kümmern muss.

Diese Lambda Function ist programmiert um Empfängerdaten aus einer Datenbank zu holen und diese mit einer Nachricht an ein Nachrichtenverteiler zu schicken.

[Mehr Infos](https://aws.amazon.com/de/lambda/)

https://www.youtube.com/watch?v=eOBq__h4OJ4
