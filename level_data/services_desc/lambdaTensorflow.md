## Lambda für Tensorflow

AWS Lambda ist ein Dienst, der eigenen Code ausführt, ohne dass man sich um die
unterliegende Server-Infrastruktur kümmern muss.

Tensorflow ist ein Framework für maschinelles Lernen, das zum Beispiel zum
Klassifizieren von Daten verwendet werden kann.

[Mehr Infos](https://aws.amazon.com/de/lambda/)

https://www.youtube.com/watch?v=eOBq__h4OJ4
