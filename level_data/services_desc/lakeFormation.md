## Lake Formation

Ein Data Lake ist eine **Ansammlung von historischen Daten**.
Die Daten werden in einem Data Lake nicht
nur in seiner Originalform, sondern auch in eine für
Datenanalyse optimierte Form gespeichert.

Lake Formation organisiert Daten in **S3**, um die Effizienz zu steigeern.

AWS Lake Formation ist ein Service, das KI-unterstützt,
ein Data Lake erschafft und auch verwaltet. So werden
Daten automatisch in Spaltenform gebracht, duplikate
entfernt, ähnliche Datensätze verlinkt, Speicher
partitioniert und verschlüsselt. 

[Mehr Infos](https://aws.amazon.com/lake-formation/)
