## DynamoDB
Amazon DynamoDB ist ein **Datenbankservice**. DynamoDB besteht aus **Tabellen** mit Elementen und Attributen.
**Kleine Daten** und Informationen können effizient abgefragt und verarbeitet werden.
Große Daten können, anders als beim S3-Bucket,
nicht direkt hochgeladen werden.
DynamoDB ist hoch skalierbar und hat extrem niedrige Latenzzeiten.
Alle Anwendungen, die einen hohen und zuverlässigen Durchsatz brauchen, können von DynamoDB profitieren.

[Mehr Infos](https://aws.amazon.com/dynamodb/)

https://www.youtube.com/watch?v=sI-zciHAh-4
