## API Gateway

Amazon API Gateway ist ein vollständig verwalteter Service, der das Erstellen, Veröffentlichen, Warten, Überwachen und Sichern von APIs für Entwickler in jeder beliebigen Größenordnung vereinfacht. Mit wenigen Klicks in der AWS-Managementkonsole können Sie eine API erstellen, die als "Haupteingang" für Anwendungen dient, um auf Daten, Geschäftslogik oder Funktionen von Ihren Backend-Services zuzugreifen, etwa Verarbeitungslasten, die auf Amazon Elastic Compute Cloud (Amazon EC2) laufen, Code, der auf AWS Lambda läuft, oder eine beliebige Webanwendung.

[Mehr Infos](https://aws.amazon.com/api-gateway/)
