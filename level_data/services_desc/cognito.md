## Cognito
Amazon Cognito ist ein **Benutzerverwaltungssystem** von Amazon. Cognito kann "User Pools" erstellen,
die den Umgang mit verschiedenen Nutzergruppen aus verschiedenen Apps erleichtert.
Neben den klassischen Log-In-Methoden, kann Cognito Nutzer auch durch etablierte Identitätsprovider,
wie Google, Facebook oder Amazon, authentifizieren.

[Mehr Infos](https://aws.amazon.com/cognito/)
