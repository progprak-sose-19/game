## IoT Core
IoT Core ist ein verwalteter **Cloud-Service**, mit dem verbundene Geräte einfach und sicher mit Cloud-Anwendungen und anderen Geräten zusammenarbeiten können. 
IoT Core kann auch bei einer großen Anzahl an Geräten helfen, Nachrichten zuverlässig und sicher zu verarbeiten und zu AWS-Endpunkten und anderen Geräten weiterzuleiten.

[Mehr Infos](https://aws.amazon.com/de/iot-core/)