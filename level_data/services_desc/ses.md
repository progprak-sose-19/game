## SES

Amazon Simple Email Service ist ein Service zum Empfangen und Versenden von E-Mails.
Er unterstützt auch die Möglichkeit, bei einer eingehenden Mail ein Event auszulösen,
das zum Beispiel ein Lambda ausführt oder die Nachricht in S3 speichert.

[Mehr Infos](https://aws.amazon.com/de/ses/)
