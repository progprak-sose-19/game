## API Gateway

API Gateway ermöglicht das Verwalten von verschiedenen APIs,
die auf weitere AWS Dienste zugreifen. Es funktioniert wie eine Art Tür,
durch der von außen ankommende Remote Procedure Calls (RPC) an die Services
im Backend weitergeleitet werden.

Zudem werden durch API Gateway sicherheitskritische Aspekte wie
Benutzerauthentifizierung, Traffic Management, Zugriffsberechtigungen
und Überwachung der Calls zentral verwaltet.

[Mehr Infos](https://aws.amazon.com/api-gateway/)