## Simple Notification System

Simple Notification System (SNS) ist ein pub/sub-System
von Amazon. Es ermöglicht das parallele Versenden von
Nachrichten an eine große Anzahl von Empfängern. Zum Versenden
stehen verschiedene Methoden zur verfügung wie mobile push, SMS oder E-Mail.

[Mehr Infos](https://aws.amazon.com/sns/)