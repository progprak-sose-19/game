## Analytics
Analytics ist ein verwalteter Service, der zur Ausführung von **Analysen** dient und das Messbarmachen von **IoT-Massendaten** erleichtert. 
Die Einblicke können helfen  bessere und fundiertere Entscheidungen für IoT- und Machine Learning-Anwendungen zu treffen.

[Mehr Infos](https://aws.amazon.com/de/iot-analytics/)