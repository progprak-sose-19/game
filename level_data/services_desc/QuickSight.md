## QuickSight
QuickSight ist ein **Business Intelligence-Service**, mit dem  Einblicke mit allen Benutzern im Unternehmen geteilt werden können. 
Mit QuickSight kann man interaktive **Dashboards** mit Machine Learning Insights erstellen und veröffentlichen. Die Dashboards können von beliebigen Geräten genützt werden oder in Anwendungen, Portale und Websites integriert werden.

[Mehr Infos](https://aws.amazon.com/de/quicksight/)