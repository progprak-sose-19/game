## AWS Shield

AWS Shield Standard und AWS Shield Advanced sind Services, die Ressourcen
von AWS-Nutzern **gegen externe Angriffe** schützen.

AWS Shield Standard wird allen Services ohne zusätzliche Kosten zur
Verfügung gestellt. Shield schützt die Webanwendungen von den
gängigsten Angriffen auf Schicht 3 und Schicht 4, den Netzwerk- und
Transportebenen.

AWS Shield Advanced bietet, so wie der Name es schon verrät, erweiterten
Schutz gegen Angriffe. Shield Advanced kann durch Früherkennung potenzieller
Angriffe diesen entgegensteuern um so die Auslastung der Services zu minimieren.
Für sehr große und umfangreiche Angriffe steht das AWS **DDoS** Response
Team rund um die Uhr zur Verfügung, um durch manuelle Migration die
Verfügbarkeit nicht zu beeinflussen. Mehrkosten die auf Grund von
Belastungsspikes eines Angriffes entstehen, werden von AWS nicht
verrechnet.

[Mehr Infos](https://aws.amazon.com/shield/)