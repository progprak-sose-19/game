## Amazon Forecast

Basieren auf der gleichen Technologie die Amazon.com benutzt,
kann Amazon Forecast Vorhersagen duchr maschinelles Lernen treffen.
Es werden historische Daten analysiert um mit großer Wahrscheinlichkeit
Vorhersagen zu treffen, wenn man davon ausgeht, dass die Zukunft
von der Vergangenheit beeinflusst wird.

Hauptanwendung dieses Services sind Läden, in denen es für die
Vorhersage von Angebot und Nachfrage verschiedener Produkte benutzt wird.

[Mehr Infos](https://aws.amazon.com/forecast/)