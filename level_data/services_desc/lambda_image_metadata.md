## Lambda für Metadaten

AWS Lambda ist ein Dienst, der eigenen Code ausführt, ohne dass man sich um die
unterliegende Server-Infrastruktur kümmern muss.

Diese Lambda Function ist programmiert um Metadaten aus einer Bilddatei zu extrahieren und in einer Datenbank zu speichern.

[Mehr Infos](https://aws.amazon.com/de/lambda/)

https://www.youtube.com/watch?v=eOBq__h4OJ4
