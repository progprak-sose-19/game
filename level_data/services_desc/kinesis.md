## Kinesis

Kinesis kann **Streaming-Daten** in Echtzeit erfassen, puffern und verarbeiten. Kinesis skaliert sich automatisch,
um sehr niedrige Latenzen zu gewährleisten. So können Sie direkt Einblicke in die Daten erhalten.
Kinesis kann Streaming-Daten von Video- und Audiodaten, Anwendungsprotokolle, Sensoren und vieles mehr erfassen.

Kinesis kann eine **Datenbank** als **Attribut** erhalten, um die Daten explizit zu speichern.

[Mehr Infos](https://aws.amazon.com/kinesis/)