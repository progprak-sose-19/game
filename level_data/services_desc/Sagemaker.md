## Sagemaker
SageMaker ermöglicht es **Modelle für maschinelles Lernen** zu erstellen, zu trainieren und zu implementieren. 
SageMaker gilt als verwalteter Service und kann den gesamten Workflow des **Machine Learning** abdeckt.

[Mehr Infos](https://aws.amazon.com/de/sagemaker/)