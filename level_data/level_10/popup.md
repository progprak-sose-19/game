## Level 10: Wunderpflanze

Der Plantagenbetreiber "FutterDas" hat von einem alten Weisen eine Wunderpflanze erhalten, die den Welthunger beenden könnte.
Leider ist dieser Alte vor einiger Zeit verstorben und nahm das Geheimnis mit ins Grab,
wie diese Wunderpflanze richtig gepflegt werden muss. Daher sind bereits 90% des Bestands der Wunderpflanze eingegangen.

Um den restlichen Bestand zu vermehren, wurde das Unternehmen "AWS Bootcamp"
damit beauftragt mithilfe von Serverless-Anwendungen die richtige Versorgung dieser Pflanze herauszufinden.
Zur Verfügung stehen Ihnen verschiedenste Sensoren, wie Lichteinfall, Feuchtigkeit, Mineralstoffe,
u.a.. Ziel ist es mithilfe von Datenanalyse und Machine Learning die optimale Versorgung der Pflanzen herauszufinden.

Die Daten der Sensoren sollen sicher gespeichert werden, der Kunde benötigt jedoch keinen Zugriff auf die Rohdaten.
Die Daten sollen visualisiert werden und dem Kunden regelmäßig auf seinem Smartphone gesendet werden.

Sie haben es in der Hand die Verbreitung der Wunderpflanze voranzutreiben und so den Welthunger zu beenden.

Viel Erfolg.
