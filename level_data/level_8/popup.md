## Level 8: Tornado-Warnsystem

Tornados verursachen große Schäden und fordern oft Menschenleben. Ein Team von Wissenschaftlern entwickelt 
ein **Warnsystem**, welches bei den richtigen Konditionen für einen Tornado eine Warnung per E-Mail an die
Wissenschaftler (Benutzer) schickt.

Zunächst müssen die Sensoren der Wetterstation die **Streaming-Daten** speichern können. Diese sollen automatisch
**analysiert** und bei Gefahr per **E-Mail** verschickt werden. Für das Analysieren der Daten können sie auch
maschinelles Lernen verwenden.
