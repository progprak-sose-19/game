## Level 1: Einführung

Willkommen, neuer Mitarbeiter, bei AWS Boot Camp!

Wir sind ein **Internetdienstleister** und bieten unseren Kunden verschiedene Web Services an.
Sie können bei uns Ihre Webseiten hosten,
über uns einen Cloud-Service anbieten, Echtzeit-Wetteranalysen berechnen und vieles mehr.
Wir setzen auf den neuen Standard: **Serverless**.
Unsere Kunden müssen sich nicht um das Verwalten von virtuelle Maschinen oder Containern kümmern.
Die lästige Arbeit nehmen wir Ihnen ab. Sie geben uns Ihren Code und wir kümmern uns
darum, dass er 24/7 läuft. Dabei muss sich der Kunde nicht einmal um das Ressourcenmanagement kümmern.
Das Beste daran: sie bezahlen nur für die Ressourcen, die sie auch tatsächlich in Anspruch nehmen.

Immer mehr Firmen wechseln zu uns und der Technik der Zukunft. Mit diesem Wechsel kommen natürlich **viele Fragen** auf.
Wir haben Sie eingestellt, um uns zu helfen all diese Fragen zu beantworten.
Um Ihnen das Themengebiet Serverless näher zubringen, haben wir einige Testaufgaben für Sie vorbereitet.
Wir zeigen Ihnen unsere **Dienstleistungen** und erklären die **Vor- und Nachteile**.

Fangen wir mit einem einfachen Beispiel an:
Die Bundespolizei (BPOL) möchte die Gesichtserkennung auf dem Südkreuz verbessern.
Sie haben aber nicht genug Rechenleistung, um den neuen Algorithmus in Echtzeit laufen zu lassen.
Deshalb hat die BPOL uns beauftragt:
Zunächst müssen wir die **Videodateien** auf unseren Server hochladen.
Dafür bietet sich ein **S3-Bucket** an. Ziehen Sie das gebrauchte Icon einfach auf den dafür vorgesehenen Slot.

Geben Sie anschließend Ihr Ergebnis mit dem **Abgabe-Button** unten links ab.

Eine hervorragende Übung!
