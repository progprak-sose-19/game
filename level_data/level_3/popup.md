## Level 3: Lambda Functions

Der Fotograf möchte nun die **Metadaten** aus den Bildern noch zusätzlich in einer Datenbank einpflegen,
um später alle Bilder finden zu können, die innerhalb eines Tages, an einem bestimmten Ort
oder mit einer bestimmten Kamera bzw. Objektiv geschossen wurden.
Jedes Mal, wenn ein Bild hochgeladen wird, wird eine Lambdafunktion getriggert,
die die Metadaten ausliest und in die Datenbank eingepflegt.

Sollten Sie nicht weiterkommen, können Sie durchs Abgeben ihrer aktuellen Lösung Hinweise erhalten.

Legen Sie los!
