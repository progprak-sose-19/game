## Level 7: Benachrichtigungen

Ein Unternehmen will bei sicherheitskritischen Systemupdates **automatisch** ihre Mitarbeiter benachrichtigen. Die Kontaktdaten der Mitarbeiter werden in eine **Datenbank** gespeichert, woraus das System die Handynummer liest und es an den **Nachrichtenverteiler** weiterleitet.
